package click.nemo.controller;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

public class SettingActivity extends Activity {


    RelativeLayout settingLayout;
    EditText txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        txtPassword = findViewById(R.id.txtPassword);
        settingLayout = findViewById(R.id.settingLayout);

        // getting Integer
        String password = preference.getDefaults("password", getApplicationContext());
        txtPassword.setText(password);

        settingLayout.setOnTouchListener(new OnSwipeTouchListener(SettingActivity.this) {
            public void onSwipeTop() {
//                Toast.makeText(MainActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                // Toast.makeText(MainActivity.this, "right", Toast.LENGTH_SHORT).show();

            }

            public void onSwipeLeft() {
//                Toast.makeText(MainActivity.this, "left", Toast.LENGTH_SHORT).show();







                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }

            public void onSwipeBottom() {
//                Toast.makeText(MainActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void changePassword(String password ) {

        final String url = URL.SWITCH_URL + "F" + password;


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("result", response);

                                preference.setDefaults("password", password, getApplicationContext()); // Storing integer

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error: Send Request Failed", Toast.LENGTH_SHORT).show();

                    }
                });
                MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

            }
        };
        new Thread(runnable).start();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }



    public void save_Clicked(View view) {

        String password = txtPassword.getText().toString();

        changePassword(password);


    }
}
