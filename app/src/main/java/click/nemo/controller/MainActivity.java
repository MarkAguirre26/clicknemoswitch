package click.nemo.controller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.thanosfisherman.wifiutils.WifiUtils;

public class MainActivity extends Activity implements View.OnTouchListener {

    private String TAG = this.getClass().getName();
    CoordinatorLayout coordinatorLayout;
    ImageButton logoButton, switch1, switch2;
    SwipeRefreshLayout pullToRefresh;
    //    ScrollView scrollView;
//    int tapCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        this.setTitle(R.string.app_name);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        logoButton = findViewById(R.id.logoButton);
        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        switch1 = findViewById(R.id.switch1);
        switch2 = findViewById(R.id.switch2);
//        scrollView = (ScrollView) findViewById(R.id.scrollView);

        WifiUtils.withContext(getApplicationContext()).enableWifi(MainActivity.this::checkResult);

        // ConnectToNetwork();

        logoButton.setOnTouchListener(this);

        setPullToRefresh();

        // ----------SWITCH 1-----------------
        setLastState();
    }

    private void setPullToRefresh() {
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Here you can update your data from internet or from local SQLite data
                pullToRefresh.setRefreshing(false);
                ConnectToNetwork();

            }
        });
    }

    private void setLastState() {
        if (preference.getDefaults(Constant.SWITCH_1, getApplicationContext()).equals(Constant.SWITCH_1_ON)) {
            switch1.setTag(Constant.SWITCH_1_ON);
            switch1.setImageResource(R.drawable.on_off_green);
        } else {
            switch1.setTag(Constant.SWITCH_1_OFF);
            switch1.setImageResource(R.drawable.on_off_gray);
        }
        // ----------SWITCH 2-----------------
        if (preference.getDefaults(Constant.SWITCH_2, getApplicationContext()).equals(Constant.SWITCH_2_ON)) {
            switch2.setTag(Constant.SWITCH_2_ON);
            switch2.setImageResource(R.drawable.on_off_green);
        } else {
            switch2.setTag(Constant.SWITCH_2_OFF);
            switch2.setImageResource(R.drawable.on_off_gray);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    void ConnectToNetwork() {
        String networkSSID = "Virtual Switch";
        String networkPass = "12345678";

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", networkSSID);
        wifiConfig.preSharedKey = String.format("\"%s\"", networkPass);

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//remember id
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();

    }


    private void checkResult(boolean isSuccess) {
        if (isSuccess) {
            showSnackBar("WIFI ENABLED", true);
        } else {
            Toast.makeText(MainActivity.this, "COULDN'T ENABLE WIFI", Toast.LENGTH_SHORT).show();
            showSnackBar("COULDN'T ENABLE WIFI", false);
        }
    }

    private void showSnackBar(String message, boolean success) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);


        // Changing message text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        if (success) {
            textView.setTextColor(Color.GREEN);
        } else {
            textView.setTextColor(Color.RED);
        }
        snackbar.show();
    }


    public void switch_Clicked(View view) {
        ImageButton btn = (ImageButton) view;
        toggleSwitch(btn);
    }

    private void setButton(ImageButton imgButton, boolean errExist) {

        String imgButtonId = getResources().getResourceEntryName(imgButton.getId());

        if (imgButtonId.equals(Constant.SWITCH_1)) {
            if (imgButton.getTag().equals(Constant.SWITCH_1_ON)) {
                imgButton.setTag(Constant.SWITCH_1_OFF);
                imgButton.setImageResource(R.drawable.on_off_gray);
                preference.setDefaults(Constant.SWITCH_1, Constant.SWITCH_1_OFF, getApplicationContext());
            } else {
                imgButton.setTag(Constant.SWITCH_1_ON);
                imgButton.setImageResource(R.drawable.on_off_green);
                preference.setDefaults(Constant.SWITCH_1, Constant.SWITCH_1_ON, getApplicationContext());

            }

            setDisbledButton(imgButton, R.drawable.on_off_gray, errExist);

        }


        if (imgButtonId.equals(Constant.SWITCH_2)) {
            if (imgButton.getTag().equals(Constant.SWITCH_2_ON)) {
                imgButton.setTag(Constant.SWITCH_2_OFF);
                imgButton.setImageResource(R.drawable.on_off_gray);
                preference.setDefaults(Constant.SWITCH_2, Constant.SWITCH_2_OFF, getApplicationContext());

            } else {
                imgButton.setTag(Constant.SWITCH_2_ON);
                imgButton.setImageResource(R.drawable.on_off_green);
                preference.setDefaults(Constant.SWITCH_2, Constant.SWITCH_2_ON, getApplicationContext());
            }

            setDisbledButton(imgButton, R.drawable.on_off_gray, errExist);


        }


    }


    void setDisbledButton(ImageButton imageButton, int resource, boolean err) {
        if (err) {
            imageButton.setImageResource(resource);
        }
    }

    void toggleSwitch(final ImageButton imgButton) {

        setButton(imgButton, false);

        String password = preference.getDefaults("password", getApplicationContext());

        final String url = URL.SWITCH_URL + imgButton.getTag() + password;


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("result", response);


                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getApplicationContext(), "Error: Send Request Failed", Toast.LENGTH_SHORT).show();
                        showSnackBar("Send Request Failed", false);
                        setButton(imgButton, true);

                    }
                });
                MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

            }
        };
        new Thread(runnable).start();
    }


    public void lazadaAds_Clicked(View view) {
        String url = "https://www.lazada.com.ph/products/virtual-switch-app-i341860866-s769576317.html?spm=a2o4l.searchlist.list.1.32bf146cmcpiLf&search=1";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            Log.d(TAG, "ACTION_BUTTON_PRESS");
            logoButton.setTag("start");
            toggleSwitch(logoButton);
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            Log.d(TAG, "ACTION_BUTTON_RELEASE");

            logoButton.setTag("stop");
            toggleSwitch(logoButton);
        }

//        if (tapCount == 7) {
//            startActivity(new Intent(getApplicationContext(), SettingActivity.class));
//            finish();
//        }
//        tapCount++;

        return true;
    }
}
